package com.example.demo.controllers;

import com.example.demo.dao.CRUDOperetions;
import com.example.demo.dao.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
public class HistoryOperationController {


    @GetMapping("history/operations")
    public ResponseEntity getHistoryOperations() {
        CRUDOperetions crudDao = new CRUDOperetions(Operation.class);
        List<Operation> operations = crudDao.getAll();
        if(operations == null){
            operations = new ArrayList<>();
        }
        return new ResponseEntity(operations, HttpStatus.OK);
    }

    @PostMapping("history/operation/add")
    public ResponseEntity addOperation(@RequestBody Operation operation) {
        if (operation != null && operation.getId() != null) {
            CRUDOperetions crudDao = new CRUDOperetions(Operation.class);
            crudDao.save(operation);
            return new ResponseEntity(operation, HttpStatus.OK);
        } else {
            return new ResponseEntity("Error: Invalid parameter  of request", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("history/operation/edit")
    public ResponseEntity editOperation(@RequestBody Operation operation) {
        if (operation != null && operation.getId() != null) {
            CRUDOperetions crudDao = new CRUDOperetions(Operation.class);
            crudDao.update(operation);
            return new ResponseEntity(operation, HttpStatus.OK);
        } else {
            return new ResponseEntity("Error: Invalid parameter  of request", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("history/operation/delete")
    public ResponseEntity deleteOperation(@PathParam("id_operation") String id_operation) {
        if (id_operation != null && id_operation.length() > 0) {
            Operation operationDelete = new Operation();
            operationDelete.setId(id_operation);
            CRUDOperetions crudDao = new CRUDOperetions(Operation.class);
            crudDao.delete(operationDelete);
            return new ResponseEntity(operationDelete.getId(), HttpStatus.OK);
        } else {
            return new ResponseEntity("Error: Invalid parameter  of request", HttpStatus.BAD_REQUEST);
        }
    }
}
