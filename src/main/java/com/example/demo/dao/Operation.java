package com.example.demo.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Operation {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Basic
    @Column(name = "result")
    private String result;

}
